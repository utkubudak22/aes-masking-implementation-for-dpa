/*
    @author: Utku Budak
    @TUM-Kennung: ge72quf
    @date: 2021 November
    This is the header file that consists of the required functions headers for the AES-128 Decryption.
*/

#ifndef __AES_DECRYPT_H__
#define __AES_DECRYPT_H__

#include <inttypes.h>

void aes128_decrypt(void *buffer);

void *aes128_init(void *key);

void mixColumnMaskCalc();

void isbox_MaskedCalc();

void mask_round_keys();

#endif
