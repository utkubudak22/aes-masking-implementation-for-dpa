/*
    @author: Utku Budak
    @TUM-Kennung: ge72quf
    @date: 2021 November
    This is the main file in order to test the AES-128 Decryption.
*/

//#include <avr/io.h>
#include <inttypes.h>
#include "aes_decrypt.h"
#include <stdio.h>

extern uint8_t key[16]; // Comment this while bringing the codes together
extern uint8_t buf[16]; // Comment this while bringing the codes together

int main()
{

    aes128_init(key);

	aes128_decrypt(buf);	// actual AES decryption

    for(uint8_t i = 0; i < 16; i++){
            printf("%02X", buf[i]);
    }

}
